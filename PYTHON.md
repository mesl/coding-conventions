# Python Conding Convention at MESL

- Generally follow [PEP-8](https://www.python.org/dev/peps/pep-0008/) except specified below.
- Recommended maximum length: 99


## Useful tools

- Pylint + Syntastic + (Neo)Vim: https://github.com/vim-syntastic/syntastic